package com.topdesk.cases.tictactoe.yoursolution;

import java.util.Optional;
import java.util.Random;

import com.topdesk.cases.tictactoe.CellLocation;
import com.topdesk.cases.tictactoe.Consultant;
import com.topdesk.cases.tictactoe.GameBoard;

import br.com.mauriciotsilva.tictactoe.Cell;
import br.com.mauriciotsilva.tictactoe.Game;
import br.com.mauriciotsilva.tictactoe.Row;
import br.com.mauriciotsilva.tictactoe.RowLocation;

public class YourConsultant implements Consultant {

	@Override
	public CellLocation suggest(GameBoard gameBoard) {

		Game game = new Game(gameBoard);
		Optional<Cell> suggestion = Optional.empty();

		for (RowLocation location : RowLocation.values()) {
			Row row = game.at(location);

			if (game.howManyEmpty() == 1) {
				suggestion = game.whichAreTheNextEmpties().stream().findFirst();
				break;
			}

			Optional<Cell> cell = suggestForWinning(row);
			if (cell.isPresent()) {
				if (row.isForkedByX()) {
					suggestion = cell;
					break;
				}

				if (row.isForkedByO()) {
					suggestion = cell;
				}
			}
		}

		return suggestion.orElseGet(() -> suggestNextMove(game)).getLocation();

	}

	private Cell suggestNextMove(Game game) {

		if (game.whichAreTheNextEmpties().isEmpty() || game.hasWinner()) {
			throw new IllegalStateException();
		}

		Cell cell = game.at(CellLocation.CENTRE_CENTRE);
		if (cell.isEmpty()) {
			return cell;
		}

		if (game.at(CellLocation.TOP_RIGHT).isEmpty()) {
			return game.at(CellLocation.TOP_RIGHT);
		}

		return game.at(CellLocation.values()[getRandomLocation()]);
	}

	private int getRandomLocation() {

		Random random = new Random();
		int result = random.nextInt(Row.MAX_ROW_NUMBER + 1);

		return result + (random.nextInt(2) == 0 ? 0 : 2);
	}

	private Optional<Cell> suggestForWinning(Row row) {

		Optional<Cell> cell = Optional.empty();
		if (row.howManyEmpties() == 1) {
			if (row.isForkedByX() || row.isForkedByO()) {
				cell = row.whichIsTheNextEmpty();
			}
		}

		return cell;
	}
}
