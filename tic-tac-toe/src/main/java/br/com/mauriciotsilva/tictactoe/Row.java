package br.com.mauriciotsilva.tictactoe;

import static com.topdesk.cases.tictactoe.CellState.EMPTY;
import static com.topdesk.cases.tictactoe.CellState.OCCUPIED_BY_O;
import static com.topdesk.cases.tictactoe.CellState.OCCUPIED_BY_X;

import java.util.Optional;
import java.util.stream.Stream;

import com.topdesk.cases.tictactoe.CellLocation;
import com.topdesk.cases.tictactoe.CellState;

public class Row {

	public static final int MAX_ROW_NUMBER = 3;

	private final int index;
	private int quantityOfCells;
	private Cell[] cells;

	private Row(int index) {
		this.index = index;
		this.quantityOfCells = 0;
		this.cells = new Cell[Cell.MAX_CELL_NUMBER];
	}

	public static Row of(CellLocation cell) {
		return new Row(cell.ordinal() / MAX_ROW_NUMBER);
	}

	public void add(CellState state) {
		Cell cell = Cell.of(this, quantityOfCells++, state);
		cells[cell.getIndex()] = cell;
	}

	public boolean is(RowLocation location) {
		return index == location.ordinal();
	}

	public boolean isForkedByX() {
		return howManyOccupiedByX() == 2;
	}

	public boolean isForkedByO() {
		return howManyOccupiedByO() == 2;
	}

	public long howManyOccupiedByX() {
		return howManyOccupiedBy(OCCUPIED_BY_X);
	}

	public long howManyOccupiedByO() {
		return howManyOccupiedBy(OCCUPIED_BY_O);
	}

	public long howManyEmpties() {
		return howManyOccupiedBy(EMPTY);
	}

	public long howManyOccupiedBy(CellState state) {
		return Stream.of(CellPosition.values()).filter(position -> at(position).is(state)).count();
	}

	public Cell at(CellPosition position) {
		return at(position.ordinal());
	}

	public Cell at(int index) {
		return cells[index];
	}

	public Optional<Cell> whichIsTheNextEmpty() {
		return Stream.of(cells).filter(cell -> cell.isEmpty()).findFirst();
	}

	protected int getIndex() {
		return index;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Row other = (Row) obj;
		if (index != other.index)
			return false;
		return true;
	}

}
