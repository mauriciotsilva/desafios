package br.com.mauriciotsilva.tictactoe;

public enum RowLocation {

	TOP, CENTER, BOTTOM;
	
}
