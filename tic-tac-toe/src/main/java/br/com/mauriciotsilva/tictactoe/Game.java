package br.com.mauriciotsilva.tictactoe;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import com.topdesk.cases.tictactoe.CellLocation;
import com.topdesk.cases.tictactoe.CellState;

import static com.topdesk.cases.tictactoe.CellState.*;
import com.topdesk.cases.tictactoe.GameBoard;

public class Game {

	private Map<Integer, Row> rows;

	public Game(GameBoard gameBoard) {
		this.rows = new HashMap<>();

		fillRows(gameBoard);
	}

	private void fillRows(GameBoard board) {

		Stream.of(CellLocation.values()).forEach(cell -> {
			Row row = get(cell);
			row.add(board.getCellState(cell));
		});
	}
	
	public boolean hasWinner() {
		return isHorizontalFull(OCCUPIED_BY_O) || isHorizontalFull(OCCUPIED_BY_X);
	}
	
	public boolean isForkedByX(){
		return isForked(OCCUPIED_BY_X);
	}
	
	public boolean isForkedByO(){
		return isForked(OCCUPIED_BY_O);
	}

	private boolean isForked(CellState state) {
		return howManyHorizontal(state) == 2;
	}

	public boolean isHorizontalFull(CellState state) {
		return howManyHorizontal(state) == Cell.MAX_CELL_NUMBER;
	}

	private long howManyHorizontal(CellState state) {
		return rows.values().stream().mapToLong(row -> row.howManyOccupiedBy(state)).sum();
	}

	private Row get(CellLocation locataion) {

		Row row = Row.of(locataion);
		if (rows.putIfAbsent(row.getIndex(), row) == null) {
			return row;
		}

		return rows.get(row.getIndex());

	}
	
	public Row at(RowLocation location){
		return at(location.ordinal());
	}
	
	private Row at(int index){
		return rows.get(index);
	}


	public long howManyEmpty() {
		return rows.values().stream().mapToLong(Row::howManyEmpties).sum();
	}

	public Collection<Cell> whichAreTheNextEmpties() {

		List<Optional<Cell>> cells = new ArrayList<>();
		for (int i = 0; i < Row.MAX_ROW_NUMBER; i++) {

			Row row = at(i);
			cells.add(row.whichIsTheNextEmpty());
		}

		return cells.stream().filter(optional -> optional.isPresent()).map(optinal -> optinal.get()).collect(toList());
	}

	public Cell at(CellLocation location) {
		int ordinal = location.ordinal();
		int row = ordinal / Row.MAX_ROW_NUMBER;

		return at(row).at(ordinal - (Cell.MAX_CELL_NUMBER * row));
	}
}
