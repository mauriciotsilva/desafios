package br.com.mauriciotsilva.tictactoe;

import static com.topdesk.cases.tictactoe.CellState.EMPTY;
import static com.topdesk.cases.tictactoe.CellState.OCCUPIED_BY_O;
import static com.topdesk.cases.tictactoe.CellState.OCCUPIED_BY_X;

import com.topdesk.cases.tictactoe.CellLocation;
import com.topdesk.cases.tictactoe.CellState;

public class Cell {

	public static final int MAX_CELL_NUMBER = 3; 

	private final Row row;
	private final int index;
	private final CellState state;

	private Cell(Row row, int index, CellState state) {
		this.row = row;
		this.index = index;
		this.state = state;
	}

	public static Cell of(Row row, int index, CellState state) {
		return new Cell(row, index, state);
	}

	public int getIndex() {
		return index;
	}

	public Row getRow() {
		return row;
	}

	public boolean isOccupiedByX() {
		return is(OCCUPIED_BY_X);
	}

	public boolean isOccupiedByO() {
		return is(OCCUPIED_BY_O);
	}

	public boolean isEmpty() {
		return is(EMPTY);
	}

	public boolean is(CellState state) {
		return this.state == state;
	}

	public CellLocation getLocation() {
		int result = row.getIndex() * Row.MAX_ROW_NUMBER;
		return CellLocation.values()[result + index];
	}

}
